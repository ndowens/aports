# Contributor: Breno Leitao <breno.leitao@gmail.com>
# Maintainer:
pkgname=py3-packaging
_pkgname=packaging
pkgver=21.2
pkgrel=0
pkgdesc="Core utilities for Python3 packages"
#options="!check" # Requires py3-pytest which requires py3-setuptools
url="https://pypi.python.org/pypi/packaging"
arch="noarch"
license="Apache-2.0 AND BSD-2-Clause"
depends="python3 py3-parsing py3-six"
# disable check to break circular dep with py3-setuptools
checkdepends="py3-pytest py3-pretend"
source="https://files.pythonhosted.org/packages/source/p/packaging/packaging-$pkgver.tar.gz"
builddir="$srcdir/$_pkgname-$pkgver"

replaces="py-packaging" # Backwards compatibility
provides="py-packaging=$pkgver-r$pkgrel" # Backwards compatibility

[ "$CARCH" = s390x ] && options="$options !check" # fails a lot

build() {
	python3 setup.py build
}

package() {
	python3 setup.py install --prefix=/usr --root="$pkgdir"
}

check() {
	PYTHONPATH="$PWD/build/lib" python3 -m pytest \
		--ignore=tests/test_manylinux.py \
		--ignore=tests/test_markers.py \
		--ignore=tests/test_specifiers.py \
		--ignore=tests/test_tags.py \
		--ignore=tests/test_version.py
	# tests that require https://pypi.org/project/pretend/ are disabled
	# above because py3-pretend hasn't been packaged for Alpine Linux
}

sha512sums="
21bdab5e529ee47a9a16d2863650120062011469e035e869fb6526ebaf61a6e8167e6a2278b74db8c15fcb9a8bc70a11c3ebe1e7ab1d677dc09ee67456d0aa1a  packaging-21.2.tar.gz
"

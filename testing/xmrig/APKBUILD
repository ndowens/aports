# Contributor: Oleg Titov <oleg.titov@gmail.com>
# Maintainer: Oleg Titov <oleg.titov@gmail.com>
pkgname=xmrig
pkgver=6.16.1
pkgrel=0
pkgdesc="XMRig is a high performance Monero (XMR) miner"
url="https://xmrig.com/"
arch="aarch64 x86 x86_64" # officially supported by upstream
license="GPL-3.0-or-later"
options="!check" # No test suite from upstream
makedepends="cmake libmicrohttpd-dev libuv-dev openssl-dev hwloc-dev"
subpackages="$pkgname-doc"
source="$pkgname-$pkgver.tar.gz::https://github.com/xmrig/xmrig/archive/v$pkgver.tar.gz
	enable-donateless-mode.patch
	"

case "$CARCH" in
	aarch64*) CMAKE_CROSSOPTS="-DARM_TARGET=8"; makedepends="$makedepends linux-headers" ;;
esac

build() {
	cmake -B build $CMAKE_CROSSOPTS -DCMAKE_BUILD_TYPE=None \
		-DWITH_CUDA=OFF \
		-DWITH_OPENCL=OFF

	make -C build
}

package() {
	install -Dm 755 build/xmrig $pkgdir/usr/bin/xmrig

	install -Dm 644 -t "$pkgdir"/usr/share/doc/$pkgname/ README.md
}

sha512sums="
f4475018ad05a5cb55d814527db273d9b0260e3fde29d572a4762acdd3db58aa0baabfea9c549fb9239c2b076c32b6956227d789ca6aaa62524b3c381617f205  xmrig-6.16.1.tar.gz
40cd7e3a884920951ec48efebbea5d7181deaeef6a226444a46ad8dc83b54eceae954c8d28952c21d63a15a4947eac72d1024b83684b5cb15437d3c8d32b006a  enable-donateless-mode.patch
"

# Contributor: Francesco Colista <fcolista@alpinelinux.org>
# Maintainer: Francesco Colista <fcolista@alpinelinux.org>
pkgname=mediastreamer2
pkgver=5.0.55
pkgrel=0
pkgdesc="library written in C that allows you to create and run audio and video streams"
url="https://www.linphone.org/technical-corner/mediastreamer2"
arch="all"
license="GPL-2.0-or-later"
options="!check" # no test available
makedepends="cmake gettext-dev ffmpeg-dev ortp-dev
	libxv-dev speex-dev speexdsp-dev libsrtp-dev
	v4l-utils-dev bctoolbox-dev mesa-dev glew-dev
	opus-dev libpcap-dev spandsp-dev tiff-dev libtheora-dev
	alsa-lib-dev python3-dev gsm-dev sqlite-dev libxml2-dev
	jpeg-dev libvpx-dev bzrtp-dev bcg729-dev"
subpackages="$pkgname-dev"
source="https://gitlab.linphone.org/BC/public/mediastreamer2/-/archive/$pkgver/mediastreamer2-$pkgver.tar.gz
	missing-def-o-binary.patch"

build() {
	cmake -B build \
		-DCMAKE_BUILD_TYPE=None \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DCMAKE_MODULE_PATH=/usr/lib/cmake \
		-DCMAKE_SKIP_INSTALL_RPATH=ON \
		-DENABLE_STRICT=NO \
		-DENABLE_ALSA=YES \
		-DENABLE_STATIC=NO \
		-DENABLE_RESAMPLE=YES \
		-DENABLE_JPEG=YES \
		-DENABLE_PCAP=YES \
		-DENABLE_FFMPEG=YES \
		-DENABLE_SPEEX_CODEC=YES \
		-DENABLE_SPEEX_DSP=YES \
		-DENABLE_SHARED=YES \
		-DENABLE_VPX=YES \
		-DENABLE_UNIT_TESTS=NO
	make -C build
}

package() {
	make -C build DESTDIR="$pkgdir" install
}

dev() {
	default_dev
	mkdir -p "$subpkgdir"/usr/lib/cmake/Mediastreamer2
	mv "$pkgdir"/usr/share/Mediastreamer2/cmake/* "$subpkgdir"/usr/lib/cmake/Mediastreamer2
	# Remove empty dirs
	rmdir "$pkgdir"/usr/share/Mediastreamer2/cmake
	rmdir "$pkgdir"/usr/share/Mediastreamer2
}

sha512sums="
534a28e1f895e43ae0890d4dfe39244104d951502585f928f88fa4f2d07ee835b6bb10b89d5455a914a858c4f3d5e3e3985b2d351fab77bee9c884b3c6b28410  mediastreamer2-5.0.55.tar.gz
d0158e03a7552580a9d9f04226c18a31a23d3cc502d52b51a9b083a5c32094ea7349495a7f74316eb727c2478a80b0385e1a28798b5b7d6e85e463485e33a822  missing-def-o-binary.patch
"

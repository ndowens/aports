# Contributor: Francesco Colista <fcolista@alpinelinux.org>
# Maintainer: Francesco Colista <fcolista@alpinelinux.org>
pkgname="icingaweb2-module-incubator"
_module=${pkgname/*-/}
pkgver=0.10.1
pkgrel=0
pkgdesc="Bleeding edge libraries useful for Icinga Web 2 modules"
url="https://www.icinga.org"
arch="noarch !armhf !armv7 !s390x !mips !mips64 !x86"
license="MIT"
options="!check"
_php=php8
depends="icingaweb2"
pkggroups="icingaweb2"
subpackages="$pkgname-doc"
source="$pkgname-$pkgver.tar.gz::https://github.com/Icinga/$pkgname/archive/v$pkgver.tar.gz"

build() {
	return 0
}

package() {
	cd "$builddir"
	mkdir -p "$pkgdir/etc/icingaweb2/modules/$_module"
	mkdir -p "$pkgdir/usr/share/doc/$pkgname"
	mkdir -p "$pkgdir/usr/share/webapps/icingaweb2/modules/$_module"
	cp -a vendor composer.json composer.lock module.info run.php \
		"$pkgdir/usr/share/webapps/icingaweb2/modules/$_module"
	chgrp -R $pkggroups "$pkgdir/etc/icingaweb2/modules/$_module"

	cat >"$pkgdir"/usr/share/doc/$pkgname/README.alpine <<EOF
You need to fix /etc/icingaweb2/modules/$_module with the owner of the user of your webserver

For nginx, as example:
    # chown -R nginx /etc/icingaweb2/modules/$_module
or
    # chown -R nobody /etc/icingaweb2/modules/$_module

For Apache:
    # chown -R apache /etc/icingaweb2/modules/$_module

For lighttpd:
    # chown -R lighttpd /etc/icingaweb2/modules/$_module

Remember to enable the module with:

    # icingacli module enable $_module

EOF

}


sha512sums="
b15cc24064356f35a8ad29f4aa932de192054e7daa9263a74d75f1baad7a3f2b2695947a624aaaf75096e3f0855e745c76cdb9d34f3ad80b60f45b033c6125bc  icingaweb2-module-incubator-0.10.1.tar.gz
"
